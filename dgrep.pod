=head1 NAME

dgrep, degrep, dfgrep, dzgrep -- grep through files belonging to an installed Debian package

=head1 SYNOPSIS

B<dgrep> [I<most grep options>] I<pattern> I<package>...

B<dgrep> B<--help>

=head1 DESCRIPTION

B<dgrep> invokes L<grep(1)> on each file in one or more installed Debian
packages.

It passes the I<package> argument(s) to L<dglob(1)> to retrieve a list of files
in those packages. You can use POSIX regular expressions for the package
names.

If B<dgrep> is invoked as B<degrep>, B<dfgrep> or B<dzgrep> then L<egrep(1)>,
L<fgrep(1)> or L<zgrep(1)> is used instead of L<grep(1)>.

=head1 OPTIONS

B<dgrep> supports most of grep(1)'s options. Please refer to your
L<grep(1)> documentation (i.e. the manpage or the texinfo manual) for
a complete listing. Only a few options are excluded because they do not
conform with the intended behaviour, see the list below.

=head2 Options of grep that are not supported by dgrep

=over 4

=item B<-r>, B<--recursive>, B<-d> I<recurse>, B<--directories>=I<recurse>

=item B<-d> I<read>, B<--directories>=I<read>

B<dgrep> searches only in the "normal" files of a package. It skips all
directories and symlinks. Therefore the options of
grep that are specific to directories are not supported.

=back

=head1 AUTHOR

Matt Zimmerman <mdz@debian.org>

This manpage was written by Frank Lichtenheld <frank@lichtenheld.de>.

=head1 COPYRIGHT AND LICENCE

Copyright (C) 2001 Matt Zimmerman <mdz@debian.org>.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

On Debian systems, a copy of the GNU General Public License version 2
can be found in F</usr/share/common-licenses/GPL-2>.

=head1 SEE ALSO

L<grep(1)>, L<egrep(1)>, L<fgrep(1)>, L<zgrep(1)>, L<dglob(1)>, L<regex(7)>, L<dpkg(8)>
